# Puppet Control Repository

## Introduction

This repository provides Puppet environments that can be deployed onto Puppet
servers using existing good practice to manage Puppet code, Hiera data and
Puppet modules.

Along with the CA Repository and ENC Repository, this repository supports
Puppet infrastructure that includes:

* _Eyaml encryption of Hiera data_ using keys created by the CA Repository
* _Trusted facts_ baked into node certificates by the CA Repository
* _Service-based ENC data_ managed by the ENC Repository
* _Jenkins_ to automate deployment of Puppet environments
* _Puppet DB_ running on a separate server from the Puppet server
* _PuppetBoard_ providing visualisation of Puppet DB data
* _Testing Puppet code_ using Hyper-V and Vagrant
* _Building the infrastructure_ from the ground up

The CA Repository is responsible for creating and deploying Puppet
node certificates onto nodes and Puppet CA data onto Puppet servers. The
repository also produces Eyaml keys for Puppet servers. These Eyaml keys
can be exported and copied into this repository, where they can be used to
encrypt Hiera data.

The ENC Repository is responsible for placing nodes into Puppet environments
and tiers. The data is stored in a collection of service definitions that declare
groups of individual Puppet nodes associated with a service.

## Encrypting Hiera Data

To encrypt Hiera data you will need to obtain the Eyaml certificate for the
Puppet server you are working with. This is stored in the CA Repository in:

    ca/<puppet server fqdn>/eyaml.crt

Copy the Eyaml certificate into the base directory of this repository.

The `encrypt` Ant target takes the place of the command-line `eyaml` executable
that is available on Puppet servers. With the `eyaml.crt` file in place, the
`encrypt` target can be used to encrypt strings or files:

    ant encrypt -Dstring=<string>
    ant encrypt -Dfile=<path to file>
    ant encrypt -Dpkcs7=<path to pkcs7 key>

The target will create a yaml file called `output.yaml` in the base
directory of this repository that will contain the encrypted data.
For example:

      ENC[PKCS7,
      MIICcAYJKoZIhvcNAQcDoIICYTCCAl0CAQAxggIpMIICJQIBADANMAACCQDYfIeK
      .
      .
      oRZ1ow==
      ]

This can be copied and pasted into the relevant Hiera file.

When encrypting the pkcs7 key you will be prompted for the master PKCS7 private
key passphrase.

## Testing Puppet Code with Hyper-V and Vagrant

Vagrant can be used with Hyper-V to test Puppet code before it is pushed onto
servers.

### Prepare Hyper-V

By default, interactions with Hyper-V will require Administrator privileges. To
remove this constraint and simplify access:

* Open Computer Management
* Go into Local Users and Groups > Groups
* Double-click on the Hyper-V Administrators group
* Add your local user

Reboot your PC for the change to come into effect.

### Hyper-V Default Virtual Switch

The 1803 Cumulative Update for Windows 10 enables the default virtual switch
defined by Hyper-V will provide VMs with DHCP-assigned IP addresses. If you
only have the default switch defined, Vagrant will automatically use it. If
you have other virtual switches defined, Vagrant will prompt you to choose
a virtual switch when you bring a VM up: in this case, be sure to choose the
default switch.

### Hyper-V and LLMNR

The Vagrantfile in this repository will configure VMs to use Microsoft's
LLMNR to manage multicast name resolution. All VMs will use the LLMNR domain
`mshome.net`.

In order for LLMNR to work the virtual switches that Hyper-V defines must be
recognised by Windows 10 as private networks. To check which category of network
the Hyper-V virtual switches are assigned to, open Control Panel and go to
Network and Internet > Network and Sharing Center.

If the Hyper-V virtual switches are listed as unidentified public networks, you
can change the network category in two ways.

The first way, which is temporary and will be lost after a reboot, is to open
PowerShell as Administrator and run:

    Set-NetConnectionProfile -InterfaceAlias 'vEthernet (Default Switch)' -NetworkCategory Private

The second way, which is permanent, is to open the Local Security Policy
application; select Network List Manager Policies; double-click on
Unidentified Networks Properties; select the Private location type then
click OK. This will require a reboot to come into effect.

Changing the local security policy on laptops is not recommended since it
will place unknown WiFi networks in the private category, which contains more
firewall exceptions than the public category.

### Prepare Eyaml Keys

Export the Vagrant Eyaml private key and certificate from the CA Repository
by running the following in the CA Repository:

    ant exporteyamlkeys -Dca=vagrant

You will be prompted to enter the CA PKCS7 passphrase and then for a separate
Eyaml private key passphrase.

This will place the Eyaml private key and certificate for Vagrant in the base
directory of the CA Repository:

* `eyaml.key`
* `eyaml.crt`

Copy these files into the base directory of this repository.

Vagrant requires access to an unencrypted version of the Eyaml private key
so run:

    ant decrypteyamlkeys

You will be prompted to enter the Eyaml private key passphrase you entered
when you exported the Eyaml keys from the CA Repository.

### Prepare Modules with r10k

Open a Command Prompt running as Administrator and run:

    choco install -y ruby

Followed by:

    gem install -y r10k

Run r10k in the base directory of this repository:

    r10k puppetfile install -v

This will download the Puppet modules defined in `Puppetfile` into
the `modules/` directory.

### Testing Roles

You can see which VMs are defined in Vagrant by running:

    vagrant status

To test a particular VM, run:

    vagrant up <vm>

This will create a Vagrant VM, copy the code that is currently
checked out in this repository and run Puppet to provision the
VM with the role that is specified in `files/vagrant-vms.yaml`.

To see what the code had done, you can connect to the Vagrant
VM by running:

    vagrant ssh <vm>

Vagrant will copy your SSH public key from the `.ssh/` directory in your home
directory. Assuming that is in place, you can also use an SSH client to
SSH to:

    vagrant@<vm>.mshome.net

To test changes made to Puppet code, run:

    vagrant rsync <vm>
    vagrant provision <vm>

This will copy the changes onto the Vagrant VM and run Puppet
again.

To remove the VM, run:

    vagrant destroy -f <vm>

### Testing Puppet Infrastructure Provisioning

Vagrant can also be used to test Puppet infrastructure provisioning. To
do this, create VMs for the Puppet server, Puppet DB server and Puppet Deploy
server as you would do for any other puppet roles:

    vagrant up puppetserver
    vagrant up puppetdb
    vagrant up puppetdeploy

These roles have `apply: false` set in `files/vagrant-vms.yaml`,
which means that the automatic Puppet provisioning step will not be performed.
Instead a provisioning account is created with correct sudo rules for
provisioning and remote access enabled using the Vagrant Puppet server SSH CA.

The steps detailed in the _Provisioning Puppet Servers and Nodes
Directly_ section can be followed to provision each of the servers.

Because Vagrant VMs sit on a private network, the use of githooks to
trigger Jenkins builds cannot be tested. However, you will be able
to trigger the builds manually by running:

    files/trigger.sh

This script looks up information about the git repositories and deploy
server in the Hiera data and uses `curl` to trigger the builds.

## Deploying Puppet Repositories With Jenkins

The `puppetdeploy` role builds a Puppet Deploy server running Jenkins that is
capable of deploying this repository, the CA Repository and the ENC Repository
onto a Puppet server. Deployments can be triggered remotely by adding a webhook
to each of the Git repositories in GitLab using the form:

    https://<puppet deploy server>/git/notifyCommit?url=<git repo url>

This will trigger any pipelines defined in Jenkins that build from the given
Git repository URL. Each webhook should trigger for push events only and SSL
verification should be disabled if Jenkins is using a self-signed certificate.

## Provisioning Puppet Servers And Nodes Directly

As well as supporting deployment of Puppet environments onto Puppet servers
using git webhooks and Jenkins pipelines, this repository provides the means
to provision Puppet server infrastructure from the ground up.

### Prepare Hiera Data

The interaction between Git repositories, Jenkins and the Puppet server
requires a significant number of Hiera variables to be added with security data
as described below.

#### Shared Data

Some puppet data is shared between the various roles so that it can be
defined in the one place.

* `profiles::puppet::agent::server`

This must be set to the FQDN of the puppet server that each Puppet agent
should connect to.

* `profiles::puppet::nodes`

This should contain a list of IP address ranges that cover all of the Puppet
nodes that will connect to the Puppet Servers.

* `profiles::puppet::servers`

This should contain a list of Puppet server FQDNs.

* `profiles::puppet::puppetdb_server`

This must be set to the FQDN of the Puppet DB server.

* `profiles::puppet::puppetdb_port`

This must be set to the high port that the Puppet DB application will
listen on.

* `profiles::puppet::deploy_server`

This must be set to the FQDN of the Puppet Deploy server.

* `profiles::puppet::puppetboard_servers`

This should contain a lost of Puppetboard server FQDNs.

#### Puppetserver Role

* `profiles::puppet::server::deploy_ssh_pubkey`

The `deploy_ssh_pubkey` is an SSH public key that will be added to the
`authorized_keys` file for the `deploy` user on the Puppet server. The
associated SSH private key must be added to the
`profiles::puppet::deploy::deploy_key` Hiera variable in the `puppetdeploy`
profile.

#### Puppetdb Role

This must contain the FQDN of the Puppet Server.

* `profiles::puppet::puppetdb::password`

The password for the PostgreSQL database that Puppet DB uses must be set for
the `puppetdb` role. This can be a long, random string that should be encrypted:

    ant encrypt -Dstring='<password>'

The same Hiera variable is used for the PostgreSQL database and application
server components of Puppet DB so the password does not need to be written
down anywhere.

#### Puppetboard Role

* `profiles::puppet::puppetboard::repo`
* `profiles::puppet::puppetboard::release`

These should be set to the Puppetboard repository and the release version
of Puppetboard that should be installed.

* `profiles::puppet::puppetboard::python_version`

This must be set to the version of Python that will be installed from the
SCL in `<major>.<minor>` format. For example: `"7.1"`

* `profiles::puppet::puppetboard::ssl_key`
* `profiles::puppet::puppetboard::ssl_cert`
* `profiles::puppet::puppetboard::ssl_chaining_cert`

Puppetboard will run using the Puppet certificate and private key for SSL but,
if needed, a different different SSL certificate and private key can be set
using these Hiera variables. The certificate and private key (and chaining
certificate, if needed) should be added using the `profiles::certs::certs`
and `profiles::certs::private_keys` hashes.

The private key should be encrypted by running:

    ant encrypt -Dfile=<path to private key>

#### Puppetdeploy Role

The `puppetdeploy` role contains data for the `profiles::jenkins` as well as
the `profiles::puppet::deploy` profiles.

* `profiles::jenkins::admin_password`
* `profiles::jenkins::admin_password_salt`

The admin password for Jenkins should be encrypted by running:

    ant encrypt -Dstring='<password>'

The salt should be generated using BCrypt. This can be done in Ruby by running:

    gem install bcrypt
    ruby -e "require 'bcrypt'; puts BCrypt::Engine.generate_salt(10);"

The salt should also be encrypted:

    ant encrypt -Dstring='<salt>'

* `profiles::jenkins::api_ssh_pubkey`
* `profiles::jenkins::api_ssh_key`

This SSH key pair is used to authenticate the Jenkins CLI that Puppet
uses to configure pipelines and credentials in Jenkins. This can be any SSH
key pair and the private key and public key can be replaced if needed.

The API ssh key should be encrypted by running:

    ant encrypt -Dfile=<path to API ssh key>

* `profiles::jenkins::ssl_key`
* `profiles::jenkins::ssl_cert`
* `profiles::jenkins::ssl_chaining_cert`

Jenkins will run using the Puppet certificate and private key for SSL but,
if needed, a different different SSL certificate and private key can be set
using these Hiera variables. The certificate and private key (and chaining
certificate, if needed) should be added using the `profiles::certs::certs`
and `profiles::certs::private_keys` hashes.

The private key should be encrypted by running:

    ant encrypt -Dfile=<path to private key>

* `profiles::jenkins::hipchat_room`
* `profiles::jenkins::hipchat_token`
* `profiles::jenkins::hipchat_uuid`

To enable HipChat notifications, generate a room notification token:

* Login to  https://uoe.hipchat.com/
* Select Rooms > My Rooms
* Select the room you want to notify
* Select Tokens
* Create a new token with the Send Notification scope

If you are not the room administrator for the room, ask the room administrator
to do this for you. To encrypt the hipchat token, run:

    ant encrypt -Dstring='<hipchat token>'

* `profiles::puppet::deploy::control_repo`
* `profiles::puppet::deploy::ca_repo`
* `profiles::puppet::deploy::enc_repo`

These should be set to the URLs of the three Git repositories.

* `profiles::puppet::deploy::repo_key`

This is an SSH private key that Jenkins will use to access the Git
repositories. The associated public key must be added as a deploy key to each
of the Git repositories.

The repo key should be encrypted by running:

    ant encrypt -Dfile=<path to repo key>

* `profiles::puppet::deploy::deploy_key`

The `deploy_key` for Jenkins is an SSH private key that Jenkins pipelines will
use to connect to the Puppet server. The associated public key must be added to
the `profiles::puppet::server::deploy_ssh_pubkey` Hiera variable for the
`puppetserver` role.

The deploy key should be encrypted by running:

    ant encrypt -Dfile=<path to deploy key>

* `profiles::puppet::deploy::pkcs7_key`

This is the PKCS7 key that the CA Repository uses to decrypt the
private keys that it creates. It is needed by the pipeline that deploys
the CA Repository onto the Puppet server.

The PKCS7 key should be encrypted by running:

    ant encrypt -Dpkcs7=<path to pkcs7 key>

You will be prompted to enter the CA PKCS7 passphrase.

* `profiles::puppet::deploy::repo_key_uuid`
* `profiles::puppet::deploy::deploy_key_uuid`
* `profiles::puppet::deploy::pkcs7_key_uuid`

These should be set to three UUIDs that will be used within Jenkins
to identify credentials. These UUIDs must match the UUIDs that are saved
in the `Jenkinsfile` in each Git repository, so that the pipelines will
fetch the correct credentials when they are triggered.

### Prepare Servers

Create three empty VMs: one for the Puppet server, one for the Puppet DB
server and one for the Puppet Deploy server.

If VMs are being provisioned for the first time the Ant target will usually
connect to the node as a user that has full sudo access. For example, on
OpenStack VMs built with CentOS the `centos` user would be used, whereas on
Vagrant VMs the `vagrant` user would be used.

Sudo access can be locked down to a number of specific commands if required.
See the _Limited Sudo Rules for Provisioning Accounts_ section for more
details.

### Prepare Repositories

Create a `custom.properties` file in the base directory of this repository
and add the following properties to match the puppet infrastructure that you
are building:

* `default.puppetserver`
* `default.domain`
* `default.tier`
* `default.platform`

For example, to build live servers on OpenStack the following properties
might be used:

    default.puppetserver=puppet.appsdev.is.ed.ac.uk
    default.domain=appsdev.is.ed.ac.uk
    default.tier=live
    default.platform=openstack

Also create a `custom.properties file in the base directory of the CA
Repository that defines the following properties:

* `default.ca`
* `default.domain`
* `default.platform`

For example:

    default.ca=appsdev
    default.domain=appsdev.is.ed.ac.uk
    default.platform=openstack

### Prepare Modules

Open a Command Prompt running as Administrator and run:

    choco install -y ruby

Followed by:

    gem install -y r10k

Run R10k in the base directory of this repository:

    r10k puppetfile install -v

This will download the Puppet modules defined in `Puppetfile` into
the `modules/` directory.

### Create CA Data

Using the CA Repository, create a CA for the new Puppet server
and sign certificates for the Puppet server, the Puppet DB server and
the Puppet Deploy server with the following roles:

* `puppetserver` for the Puppet server
* `puppetdb` for the Puppet DB server
* `puppetdeploy` for the Puppet Deploy server

For example, to set up a CA for OpenStack:

    ant newca -Dca=appsdev
    ant sign -Dca=appsdev -Dnode=puppet.appsdev.is.ed.ac.uk -Drole=puppetserver
    ant sign -Dca=appsdev -Dnode=puppetdb.appsdev.is.ed.ac.uk -Drole=puppetdb
    ant sign -Dca=appsdev -Dnode=puppetdeploy.appsdev.is.ed.ac.uk -Drole=puppetdeploy

Please review the the CA Repository README for more details.

### Prepare Eyaml Keys

Before provisioning you must export the Eyaml private key and certificate for
the Puppet infrastructure from the CA Repository. Run the following in the
CA Repository:

    ant exporteyamlkeys -Dca=<ca name>

You will be prompted to enter the CA PKCS7 passphrase and then for a separate
Eyaml private key passphrase.

Copy the `eyaml.key` and `eyaml.crt` files from the base directory of the CA
Repository into the base directory of this repository. See the _Exporting
Eyaml Keys_ section in the CA Repository README for more details.

### Prepare SSH Keys

You may also need to sign your SSH public key using the SSH CA to gain
temporary access to the provisioning account on the remote server. Use the
`signsshkey` target in the CA Repository to do this. For example:

    ant signsshkey

See the _Using the SSH CA_ section in the CA Repository README for more
details.

### Provision CA Data

Use the `provision` Ant target in the CA Repository to provision each
of the servers. This will install the correct Puppet packages and deploy the
required certificates and keys from the CA Repository. For example:

    ant provision -Duser=<provisioning account> -Dnode=puppet.appsdev.is.ed.ac.uk
    ant provision -Duser=<provisioning account> -Dnode=puppetdb.appsdev.is.ed.ac.uk
    ant provision -Duser=<provisioning account> -Dnode=puppetdeploy.appsdev.is.ed.ac.uk

See the _Using the SSH CA_ section in the CA Repository README for more
details.

### Provision Puppet Roles

Use the `provision` Ant target this repository to provision each
of the servers with the required Puppet role. For example:

    ant provision -Drole=puppetserver -Duser=<provisioning account> -Dnode=puppet.appsdev.is.ed.ac.uk
    ant provision -Drole=puppetdb -Duser=<provisioning account> -Dnode=puppetdb.appsdev.is.ed.ac.uk
    ant provision -Drole=puppetdeploy -Duser=<provisioning account> -Dnode=puppetdeploy.appsdev.is.ed.ac.uk

You will be prompted to enter the Eyaml private key passphrase you entered
when you exported the Eyaml keys from the CA Repository.

At this point the provisioning account has done its work. Assuming that you
are an admin user, you will be able to complete provisioning using your own
account on the servers.

The provisioning target must be run once more for the `puppetserver` and
`puppetdb` roles and twice more for the `puppetdeploy` role. For example:

    ant provision -Drole=puppetserver -Dnode=puppet.appsdev.is.ed.ac.uk
    ant provision -Drole=puppetdb -Dnode=puppetdb.appsdev.is.ed.ac.uk
    ant provision -Drole=puppetdeploy -Dnode=puppetdeploy.appsdev.is.ed.ac.uk -Druns=3

You will be prompted to enter the Eyaml private key passphrase you entered
when you exported the Eyaml keys from the CA Repository.

The second provisioning run clears default firewall rules on each of the
servers.

The Deploy Server requires four provisioning runs in total because of the way
Puppet manages Jenkins plugins, credentials and pipelines: the first run will
install Jenkins; the second run will install the main configuration files; the
third run will install the require plugins and the fourth run will configure
the credentials and pipelines.

Once the pipelines are created they will be triggered once by Puppet, causing
each of the Git repositories to be deployed onto the Puppet server for the
first time.

### Test Puppet Agent

At this point, the Puppet infrastructure should be operational. SSH into
each of servers and run:

    sudo puppet agent -tv

The Puppet agent run should complete with no errors.

### Configure Webhooks in GitLab

With the provisioning complete, webhooks can be added to GitLab for this
repository, the CA Repository and the ENC Repository. See the
_Deploying Puppet Repositories With Jenkins_ above for more details.

## Limited Sudo Rules for Provisioning Accounts

Rather than relying on a remote account that has full sudo access, the sudo
rules listed in the `files/sudorules` file can be used to support the Ant
provisioning process.

Note that Puppet will remove any sudo rules that it has not configured itself
so these rules will only be in place until the provisioning process is run for
the first time.

### Testing Limited Sudo Rules

Vagrant adds a separate `sudorules` account that has the limited set of sudo
rules applied when it builds Puppet infrastructure nodes. The sudo rules can
be tested by specifying the `sudorules` user as the provisioning account rather
than `vagrant` when provisioning the Vagrant Puppet infrastructure nodes.
