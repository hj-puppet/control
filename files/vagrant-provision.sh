#!/bin/sh

# set default facts
export FACTER_role="$ROLE"
export FACTER_tier="vagrant"
export FACTER_location="vagrant"

# add the user's SSH public key
if [ -n "${SSH_PUB_KEY}" ]
then
  grep "${SSH_PUB_KEY}" /home/vagrant/.ssh/authorized_keys >& /dev/null
  if [ $? -gt 0 ]
  then
    echo "${SSH_PUB_KEY}" >> /home/vagrant/.ssh/authorized_keys
  fi
fi

# enable LLMNR
rpm -q systemd-resolved >& /dev/null
if [ $? -gt 0 ]
then
  yum install -y systemd-resolved
  sed -i 's/.*LLMNR=.*/LLMNR=yes/' /etc/systemd/resolved.conf
  systemctl restart systemd-resolved
fi

# enable NTP
rpm -q ntp >& /dev/null
if [ $? -gt 0 ]
then
  yum install -y ntp
  systemctl start ntpd
  systemctl enable ntpd
  ln -fs /usr/share/zoneinfo/Europe/London /etc/localtime
fi

# either install Puppet and run puppet apply or configure for provisioning
if [ "${PUPPET_APPLY}" == "true" ]
then

  # install the Puppet 5 repo
  rpm -q puppet5-release >& /dev/null
  if [ $? -gt 0 ]
  then
    RELEASE=`awk 'BEGIN { FS="[. ]" } { print $4 }' /etc/redhat-release`
    RPM="puppet5-release-el-${RELEASE}.noarch.rpm"
    rpm -ihv "https://yum.puppetlabs.com/puppet5/${RPM}"
  fi

  # install puppet agent
  rpm -q puppet-agent >& /dev/null
  if [ $? -gt 0 ]
  then
    yum install -y puppet-agent
  fi

  # generate dummy certificate
  if [ ! -d /etc/puppetlabs/puppet/ssl ]
  then
    /opt/puppetlabs/bin/puppet cert generate $HOSTNAME
  fi

  # run puppet
  /opt/puppetlabs/bin/puppet apply /vagrant/manifests/site.pp \
  --modulepath /vagrant/site:/vagrant/modules \
  --hiera_config /vagrant/files/vagrant-hiera.yaml

else

  # create the sshca and sudorules users for testing
  id sshca >& /dev/null
  if [ $? -gt 0 ]
  then
    useradd -G vagrant -m sshca
  fi
  id sudorules >& /dev/null
  if [ $? -gt 0 ]
  then
    useradd -m sudorules
  fi

  # install the vagrant authorized_keys for the sudorules user
  if [ ! -d /home/sudorules/.ssh ]
  then
    mkdir /home/sudorules/.ssh
    chmod 0700 /home/sudorules/.ssh
    chown sudorules:sudorules /home/sudorules/.ssh
  fi
  cp /home/vagrant/.ssh/authorized_keys /home/sudorules/.ssh/authorized_keys
  chmod 0600 /home/sudorules/.ssh/authorized_keys
  chown sudorules:sudorules /home/sudorules/.ssh/authorized_keys

  # install the SSH CA public key for the sshca user
  if [ -n "${SSHCA_PUB_KEY}" ]
  then
    if [ ! -d /home/sshca/.ssh ]
    then
      mkdir /home/sshca/.ssh
      chmod 0700 /home/sshca/.ssh
      chown sshca:sshca /home/sshca/.ssh
    fi
    grep "${SSHCA_PUB_KEY}" /home/sshca/.ssh/authorized_keys >& /dev/null
    if [ $? -gt 0 ]
    then
      echo "cert-authority ${SSHCA_PUB_KEY}" > /home/sshca/.ssh/authorized_keys
      chmod 0600 /home/sshca/.ssh/authorized_keys
      chown sshca:sshca /home/sshca/.ssh/authorized_keys
    fi
  fi

  # install the sudo rules for the sudorules user and check that they are valid
  cp /vagrant/files/sudorules /etc/sudoers.d/sudorules
  chmod 440 /etc/sudoers.d/sudorules
  visudo -c -f /etc/sudoers.d/sudorules >& /dev/null
  if [ $? -gt 0 ]
  then
    rm -f /etc/sudoers.d/sudorules
  fi

fi
