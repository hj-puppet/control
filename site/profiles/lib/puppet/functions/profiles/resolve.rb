require 'resolv'
#
# Performs a name resolution.
#
Puppet::Functions.create_function(:'profiles::resolve') do

  dispatch :resolve_string do
    required_param 'String', :ip
    return_type    'String'
  end

  def resolve_string(ip)
    if ( ip =~ /^\d+\.\d+\.\d+\.\d+(\/\d+)?$/ )
      return ip
    else
      r = Resolv.new(resolvers=[Resolv::DNS.new])
      r.getaddress(ip)
    end
  end

end
