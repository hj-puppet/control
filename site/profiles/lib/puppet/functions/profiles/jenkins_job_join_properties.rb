#
# Turns hash into "key=value key=value ...etc" string
#
Puppet::Functions.create_function(:'profiles::jenkins_job_join_properties') do

  dispatch :join_properties_hash do
    required_param 'Hash', :properties
    return_type    'String'
  end

  def join_properties_hash(properties)
    keyvalues = []
    properties.each do |key, value|
      keyvalues.push("#{key}=#{value}")
    end
    return keyvalues.join(' ')
  end

end
