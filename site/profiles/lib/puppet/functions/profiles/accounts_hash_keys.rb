#
# Returns a hash structure suitable for creating ssh_authorized_keys
# defined types.
#
Puppet::Functions.create_function(:'profiles::accounts_hash_keys') do

  dispatch :hash_authorized_keys_string do
    required_param 'String', :user
    required_param 'String', :authorized_key
    return_type    'Hash'
  end

  dispatch :hash_authorized_keys_array_string do
    required_param 'String', :user
    required_param 'Array[String]', :authorized_keys
    return_type    'Hash'
  end

  def hash_authorized_keys_string(user, authorized_key)
    hash = Hash.new
    parts = authorized_key.split
    comment = "#{user}_0"
    hash[comment] = Hash.new
    hash[comment]['type'] = parts[0]
    hash[comment]['key'] = parts[1]
    return hash
  end

  def hash_authorized_keys_array_string(user, authorized_keys)
    count = 0
    hash = Hash.new
    authorized_keys.each do |key|
      parts = key.split
      comment = "#{user}_#{count}"
      hash[comment] = Hash.new
      hash[comment]['type'] = parts[0]
      hash[comment]['key'] = parts[1]
      count += 1
    end
    return hash
  end

end
