#
# Custom fact that contains the version of Jenkins that has been installed.
#
Facter.add('jenkins_version') do
  setcode do
    installutilfile = '/var/lib/jenkins/jenkins.install.InstallUtil.lastExecVersion'
    if File.exist? installutilfile
      # get the first line of the file
      File.open(installutilfile, 'r') { |f| f.readline }
    else
      upgradewizardfile = '/var/lib/jenkins/jenkins.install.UpgradeWizard.state'
      if File.exist? upgradewizardfile
        # get the first line of the file
        File.open(upgradewizardfile, 'r') { |f| f.readline }
      end
    end
  end
end