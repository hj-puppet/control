#!/bin/sh
export GIT_SSH=/usr/local/gitssh/gitssh.sh
HEAD=`git rev-parse HEAD`
REMOTE=`git ls-remote origin -h refs/heads/$1`
if [ "$REMOTE" != "" ]
then
  echo $REMOTE | grep "^$HEAD " > /dev/null
  exit $?
fi
exit 0