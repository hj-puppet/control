#!/usr/bin/ruby

require 'yaml'

# location of the enc directory
enc_dir = "/etc/puppetlabs/code/enc"

# enc hash with default values
enc = {
  'environment' => 'production',
  'parameters'  => {}
}

# get the enc file for the requested node
fqdn = ARGV[0]
parts = fqdn.split('.')
hostname = parts.shift
domain = parts.join('.')
node_file = "#{enc_dir}/#{domain}/#{hostname}.yaml"

# load data into the enc hash
if File.exists?(node_file)
  node=YAML.load_file(node_file)
  if node.is_a?(Hash)
    node.each do |name, value|
      if name == 'environment'
        environment = value.gsub(/[^A-Za-z0-9]+/, '_').downcase
        if environment == 'master'
          environment = 'production'
        end
        enc['environment'] = environment
      else
        enc['parameters'][name] = value
      end
    end
  end
end

# return data in yaml format
puts enc.to_yaml
