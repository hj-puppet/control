<%-|
  $repo,
  $repo_uuid,
  $branch,
  $authtoken,
  $when,
  $target,
  $properties,
  $hipchat,
  $delete_workspace,
  $files,
  $timeout
|-%>
<?xml version='1.1' encoding='UTF-8'?>
<project>
  <actions/>
  <description></description>
  <keepDependencies>false</keepDependencies>
  <properties/>
  <scm class="hudson.plugins.git.GitSCM" plugin="git@3.9.1">
    <configVersion>2</configVersion>
    <userRemoteConfigs>
      <hudson.plugins.git.UserRemoteConfig>
        <url><%= $repo %></url>
        <credentialsId><%= $repo_uuid %></credentialsId>
      </hudson.plugins.git.UserRemoteConfig>
    </userRemoteConfigs>
    <branches>
      <hudson.plugins.git.BranchSpec>
        <name><%= $branch %></name>
      </hudson.plugins.git.BranchSpec>
    </branches>
    <doGenerateSubmoduleConfigurations>false</doGenerateSubmoduleConfigurations>
    <submoduleCfg class="list"/>
    <extensions/>
  </scm>
  <canRoam>true</canRoam>
  <disabled>false</disabled>
  <blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding>
  <blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding>
<% if $authtoken { -%>
  <authToken>${authtoken}</authToken>
<% } -%>
<% if $when { -%>
  <triggers>
    <hudson.triggers.TimerTrigger>
      <spec><%= $when %></spec>
    </hudson.triggers.TimerTrigger>
  </triggers>
<% } -%>
  <concurrentBuild>false</concurrentBuild>
  <builders>
    <hudson.tasks.Ant plugin="ant@1.8">
      <targets><%= $target %></targets>
      <properties><%= $properties %></properties>
    </hudson.tasks.Ant>
  </builders>
<% if $hipchat { -%>
  <publishers>
    <jenkins.plugins.hipchat.HipChatNotifier plugin="hipchat@2.1.1">
      <credentialId></credentialId>
      <room></room>
      <notifications>
        <jenkins.plugins.hipchat.model.NotificationConfig>
          <notifyEnabled>false</notifyEnabled>
          <textFormat>false</textFormat>
          <notificationType>STARTED</notificationType>
          <color>YELLOW</color>
          <messageTemplate>Started &lt;strong&gt;${JOB_NAME}&lt;/strong&gt; build &lt;strong&gt;#${BUILD_NUMBER}&lt;/strong&gt; - &lt;a href="${BUILD_URL}console"&gt;view console&lt;/a&gt;</messageTemplate>
        </jenkins.plugins.hipchat.model.NotificationConfig>
        <jenkins.plugins.hipchat.model.NotificationConfig>
          <notifyEnabled>false</notifyEnabled>
          <textFormat>false</textFormat>
          <notificationType>SUCCESS</notificationType>
          <color>GREEN</color>
          <messageTemplate>Finished &lt;strong&gt;${JOB_NAME}&lt;/strong&gt; build &lt;strong&gt;#${BUILD_NUMBER}&lt;/strong&gt; - &lt;a href="${BUILD_URL}"&gt;view build&lt;/a&gt;</messageTemplate>
        </jenkins.plugins.hipchat.model.NotificationConfig>
        <jenkins.plugins.hipchat.model.NotificationConfig>
          <notifyEnabled>false</notifyEnabled>
          <textFormat>false</textFormat>
          <notificationType>BACK_TO_NORMAL</notificationType>
          <color>GREEN</color>
          <messageTemplate>Finished &lt;strong&gt;${JOB_NAME}&lt;/strong&gt; build &lt;strong&gt;#${BUILD_NUMBER}&lt;/strong&gt; - &lt;a href="${BUILD_URL}"&gt;view build&lt;/a&gt;</messageTemplate>
        </jenkins.plugins.hipchat.model.NotificationConfig>
        <jenkins.plugins.hipchat.model.NotificationConfig>
          <notifyEnabled>false</notifyEnabled>
          <textFormat>false</textFormat>
          <notificationType>FAILURE</notificationType>
          <color>RED</color>
          <messageTemplate>Failed &lt;strong&gt;${JOB_NAME}&lt;/strong&gt; build &lt;strong&gt;#${BUILD_NUMBER}&lt;/strong&gt; - &lt;a href="${BUILD_URL}console"&gt;view console&lt;/a&gt;</messageTemplate>
        </jenkins.plugins.hipchat.model.NotificationConfig>
        <jenkins.plugins.hipchat.model.NotificationConfig>
          <notifyEnabled>false</notifyEnabled>
          <textFormat>false</textFormat>
          <notificationType>ABORTED</notificationType>
          <color>RED</color>
          <messageTemplate>Failed &lt;strong&gt;${JOB_NAME}&lt;/strong&gt; build &lt;strong&gt;#${BUILD_NUMBER}&lt;/strong&gt; - &lt;a href="${BUILD_URL}console"&gt;view console&lt;/a&gt;</messageTemplate>
        </jenkins.plugins.hipchat.model.NotificationConfig>
        <jenkins.plugins.hipchat.model.NotificationConfig>
          <notifyEnabled>false</notifyEnabled>
          <textFormat>false</textFormat>
          <notificationType>UNSTABLE</notificationType>
          <color>RED</color>
          <messageTemplate>Failed &lt;strong&gt;${JOB_NAME}&lt;/strong&gt; build &lt;strong&gt;#${BUILD_NUMBER}&lt;/strong&gt; - &lt;a href="${BUILD_URL}console"&gt;view console&lt;/a&gt;</messageTemplate>
        </jenkins.plugins.hipchat.model.NotificationConfig>
      </notifications>
      <startJobMessage></startJobMessage>
      <completeJobMessage></completeJobMessage>
    </jenkins.plugins.hipchat.HipChatNotifier>
  </publishers>
<% } else { -%>
  <publishers/>
<% } -%>
  <buildWrappers>
<% if $delete_workspace { -%>
    <hudson.plugins.ws__cleanup.PreBuildCleanup plugin="ws-cleanup@0.34">
      <deleteDirs>false</deleteDirs>
      <cleanupParameter></cleanupParameter>
      <externalDelete></externalDelete>
    </hudson.plugins.ws__cleanup.PreBuildCleanup>
<% } -%>
<% if $files { -%>
    <org.jenkinsci.plugins.credentialsbinding.impl.SecretBuildWrapper plugin="credentials-binding@1.16">
      <bindings>
<% each ($files) |$variable, $uuid| { -%>
        <org.jenkinsci.plugins.credentialsbinding.impl.FileBinding>
          <credentialsId><%= $uuid %></credentialsId>
          <variable><%= $variable %></variable>
        </org.jenkinsci.plugins.credentialsbinding.impl.FileBinding>
<% } -%>
      </bindings>
    </org.jenkinsci.plugins.credentialsbinding.impl.SecretBuildWrapper>
<% } -%>
    <hudson.plugins.build__timeout.BuildTimeoutWrapper plugin="build-timeout@1.19">
      <strategy class="hudson.plugins.build_timeout.impl.AbsoluteTimeOutStrategy">
        <timeoutMinutes><%= $timeout %></timeoutMinutes>
      </strategy>
      <operationList/>
    </hudson.plugins.build__timeout.BuildTimeoutWrapper>
    <hudson.plugins.timestamper.TimestamperBuildWrapper plugin="timestamper@1.8.10"/>
    <hudson.tasks.AntWrapper plugin="ant@1.8"/>
  </buildWrappers>
</project>
