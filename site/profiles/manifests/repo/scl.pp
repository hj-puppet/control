# == Class: profiles::repo::scl

class profiles::repo::scl () {

  case $facts['os']['name'] {

    'CentOS': {

      # add the SCL repo
      package { 'centos-release-scl':
        ensure => 'present',
        notify => Exec['profiles-repo-scl-ycm']
      }

      # enable the SCL repo
      exec { 'profiles-repo-scl-ycm':
        path        => [ '/usr/bin', '/usr/sbin' ],
        command     => 'yum-config-manager --enable rhel-server-rhscl-7-rpms',
        refreshonly => true
      }

    }

    default: {
      fail("Class profiles::repo::scl is not supported on ${facts['os']['name']}")
    }

  }

}

