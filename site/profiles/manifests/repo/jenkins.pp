# == Class: profiles::repo::jenkins

class profiles::repo::jenkins () {

  # import the gpg key
  profiles::repo::gpg { 'jenkins': }

  # add the repo
  yumrepo { 'jenkins':
    baseurl  => 'http://pkg.jenkins.io/redhat-stable',
    descr    => 'Jenkins-stable',
    enabled  => true,
    gpgcheck => true,
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-jenkins',
    require  => Profiles::Repo::Gpg['jenkins']
  }

}
