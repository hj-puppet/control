# == Class: profiles::repo::puppet5

class profiles::repo::puppet5 () {

  # import the gpg key
  profiles::repo::gpg { 'puppet5': }

  # add the PuppetLabs repo
  yumrepo { 'puppet5':
    baseurl  => "http://yum.puppetlabs.com/puppet5/el/${facts['os']['release']['major']}/\$basearch",
    descr    => "Puppet 5 Repository el ${facts['os']['release']['major']} - \$basearch",
    enabled  => true,
    gpgcheck => true,
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-puppet5',
    require  => Profiles::Repo::Gpg['puppet5']
  }

}
