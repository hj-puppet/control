# == Class: profiles::admin

class profiles::admin () {

  # profile lookups
  $admin_groups = lookup('profiles::admin::groups',  Array, 'unique', [])
  $admin_pcs    = lookup('profiles::admin::pcs',     Array, 'unique', [])

  # open the firewall for ssh from admin PCs
  profiles::firewall::rule { 'SSH from admin PCs':
    dport  => 22,
    proto  => 'tcp',
    source => $admin_pcs
  }

  each ($admin_groups) |$index, $group| {

    # allow admins to work with puppet agent
    profiles::sudo::rule { "admin_${group}_root_puppet_agent":
      group    => $group,
      as       => 'root',
      nopasswd => true,
      commands => [
        '/bin/systemctl start puppet',
        '/bin/systemctl stop puppet',
        '/opt/puppetlabs/bin/puppet agent -tv',
        '/opt/puppetlabs/bin/puppet agent -tv --noop',
        '/opt/puppetlabs/bin/puppet agent --disable',
        '/opt/puppetlabs/bin/puppet agent --enable'
      ]
    }

    # allow admins to work with puppet apply for provisioning (requires setenv)
    profiles::sudo::rule { "admin_${group}_root_puppet_apply":
      group    => $group,
      as       => 'root',
      nopasswd => true,
      setenv   => true,
      commands => [
        '/opt/puppetlabs/bin/puppet apply *'
      ]
    }

    # allow admins to provision CA data and puppet agent config
    profiles::sudo::rule { "admin_${group}_root_provision":
      group    => $group,
      as       => 'root',
      nopasswd => true,
      commands => [
        '/bin/rsync --server -[lDtre.iLsfxC]* --delete --new-compress . /etc/puppetlabs/puppet/ssl',
        '/bin/rsync --server -[lDtre.iLsfxC]* --delete --new-compress . /etc/puppetlabs/puppet/puppet.conf'
      ]
    }

    # allow admins to check iptables
    profiles::sudo::rule { "admin_${group}_root_iptables":
      group    => $group,
      as       => 'root',
      nopasswd => true,
      commands => [
        '/usr/sbin/iptables -L *'
      ]
    }
  }

  # install support packages
  package { [
    'net-tools',
    'nc',
    'lsof'
  ]:
    ensure => 'present'
  }

  # ensure that the root .ssh directory is declared
  file { '/root/.ssh':
    ensure => 'directory',
    owner  => 'root',
    group  => 'root',
    mode   => '0600',
  }
}
