# == Class: profiles::package::ruby

class profiles::package::ruby () {

  package { 'ruby':
    ensure => 'present'
  }

}
