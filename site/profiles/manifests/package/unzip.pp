# == Class: profiles::package::unzip

class profiles::package::unzip () {

  package { 'unzip':
    ensure => 'present'
  }

}
