# == Defined Type: profiles::jenkins::pipeline
#
# Adds a multibranch pipeline. Can be triggered with a git hook using the
# URL: https://<jenkins server>/git/notifyCommit?url=<git repo url>?delay=0sec
#
# === Parameters
#
# [*name*]
#   (Namevar) Name of the job.
#
# [*repo*]
#   (Required) URL of the Git repo.
#
# [*repo_uuid*]
#   (Required) UUID of Jenkins credentials to use when connecting to the Git repo.
#
# [*branches*]
#   (Optional, default 'production') Regular expression used to filter detected
#   branches. 
#
# [*jenkinsfile*]
#   (Optional, default 'Jenkinsfile') Name of the Jenkinsfile to run.
#
# [*hours*]
#   (Optional, default undef) If defined the pipeline will be triggered every
#   N hours, where N is the specified integer.
#

define profiles::jenkins::pipeline (
  String $repo,
  String $repo_uuid,
  Optional[String]  $branches    = 'production',
  Optional[String]  $jenkinsfile = 'Jenkinsfile',
  Optional[Integer] $hours       = undef,
) {

  $xml = "/etc/jenkins/pipelines/${name}.xml"

  if $hours {
    $triggers = epp('profiles/jenkins/pipeline-trigger-periodic.xml.epp', {
      'milliseconds' => $hours * 60 * 60 * 1000,
    })
  } else {
    $triggers = '  <triggers/>'
  }

  # create the job xml file
  file { $xml:
    ensure  => 'present',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => epp('profiles/jenkins/pipeline.xml.epp', {
      'repo'        => $repo,
      'repo_uuid'   => $repo_uuid,
      'branches'    => $branches,
      'triggers'    => $triggers,
      'jenkinsfile' => $jenkinsfile,
      'scm_uuid'    => fqdn_uuid("${name}.${facts['fqdn']}")
    })
  }

  # work out the cli arguments
  $jar    = '/usr/lib/jenkins/jenkins-cli.jar'
  $site   = 'http://127.0.0.1:8080/'
  $auth   = '-ssh -user admin -i /etc/jenkins/api_id_rsa'
  $cli    = "/usr/bin/java -jar ${jar} -s ${site} ${auth}"

  # create the job for the first time
  exec { "profiles-jenkins-pipeline-create-${name}":
    command => "${cli} create-job ${name} < ${xml}",
    unless  => "${cli} get-job ${name}",
    onlyif  => [
      '/usr/bin/test -d /var/lib/jenkins/plugins/workflow-multibranch',
      '/usr/bin/curl -fsSI http://127.0.0.1:8080/cli/'
    ],
    require => File[$xml],
    notify  => Exec["profiles-jenkins-pipeline-trigger-${name}"]
  }

  # trigger a scan when the pipeline is updated
  exec { "profiles-jenkins-pipeline-trigger-${name}":
    command     => "/usr/bin/curl http://127.0.0.1:8080/git/notifyCommit?url=${repo}",
    onlyif      => [
      '/usr/bin/test -d /var/lib/jenkins/plugins/git',
      '/usr/bin/curl -fsSI http://127.0.0.1:8080/cli/'
    ],
    refreshonly => true,
  }

}

