# == Defined Type: profiles::jenkins::file

define profiles::jenkins::file (
  String $uuid,
  String $content,
) {

  $xml = "/etc/jenkins/files/${name}.xml"

  # encode the content
  $encoded = base64('encode', $content, 'urlsafe')

  # create the xml for creating the credential
  file { $xml:
    ensure  => 'present',
    owner   => 'jenkins',
    group   => 'jenkins',
    mode    => '0600',
    content => epp('profiles/jenkins/file.xml.epp', {
      'uuid'    => $uuid,
      'name'    => $name,
      'content' => $encoded
    }),
    require => File['/etc/jenkins/files']
  }

  # work out the cli arguments
  $jar    = '/usr/lib/jenkins/jenkins-cli.jar'
  $site   = 'http://127.0.0.1:8080/'
  $auth   = '-ssh -user admin -i /etc/jenkins/api_id_rsa'
  $create = 'create-credentials-by-xml "SystemCredentialsProvider::SystemContextResolver::jenkins" "(global)"'
  $check  = "get-credentials-as-xml system::system::jenkins _ ${uuid}"

  # install the credentials for the first time
  exec { "profiles-jenkins-file-create-${name}":
    command => "/usr/bin/java -jar ${jar} -s ${site} ${auth} ${create} < ${xml}",
    unless  => "/usr/bin/java -jar ${jar} -s ${site} ${auth} ${check}",
    onlyif  => [
      '/usr/bin/test -d /var/lib/jenkins/plugins/credentials',
      '/usr/bin/curl -fsSI http://127.0.0.1:8080/cli/'
    ],
    require => File[$xml]
  }

}
