# == Defined Type: profiles::jenkins::job
#
# Adds a job that builds an Ant target. Can be triggered at set times or with
# a git hook using the URL:
#   https://<jenkins server>/job/<name>/build?token=<authtoken>
#
# === Parameters
#
# [*name*]
#   (Namevar) Name of the job.
#
# [*repo*]
#   (Required) URL of the Git repo Ant will build from.
#
# [*repo_uuid*]
#   (Required) UUID of Jenkins credentials to use when connecting to the Git repo.
#
# [*branch*]
#   (Required) The branch Ant will build from.
#
# [*target*]
#   (Required) The Ant target that will be built.
#
# [*hipchat*]
#   (Optional, default false) If defined a notification will be sent to the
#   HipChat room defined in Jenkins top level configuration when the job starts
#   and finishes.
#
# [*delete_workspace*]
#   (Optional, default false) Set to true if the existing workspace should be
#   deleted before each build.
#
# [*properties*]
#   (Optional, default empty) Hash of name/value pairs that will be added to
#   the ant command line as Java properties e.g. -Dname1=value1 -Dname2=value2
#
# [*files*]
#   (Optional, default empty) Hash of variable/uuid pairs for secret files
#   defined by profiles::jenkins::file that will be made available to the
#   ant build as environment variables. For example:
#     { 'MY_KEY' => '00145-135-727' }
#   Will add the path to the secret file associated with uuid '00145-135-727'
#   to the environment variable 'MY_KEY', which can be accessed in Ant as
#   ${env.MY_KEY}.
#
# [*timeout*]
#   (Optional, default 3) Number of minutes after which a job run will be
#   aborted if it has not completed.
#
# [*when*]
#   (Optional, default undefined) If defined, the job will be triggered at
#   a set time based on the value of this parameter, which follows the cron
#   standard. E.g.: 2 23 * * * would run at 23:02 every day.
#
# [*authoken*]
#   (Optional, default undefined) If defined, the job can be triggered by
#   hitting the URL https://<jenkins server>/job/<name>/build?token=<authtoken>.
#

define profiles::jenkins::job (
  String            $repo,
  String            $repo_uuid,
  String            $branch,
  String            $target,
  Optional[String]  $authtoken        = undef,
  Optional[String]  $when             = undef,
  Optional[Hash]    $properties       = {},
  Optional[Boolean] $hipchat          = false,
  Optional[Boolean] $delete_workspace = false,
  Optional[Hash]    $files            = undef,
  Optional[Integer] $timeout          = 3
) {

  $xml = "/etc/jenkins/jobs/${name}.xml"

  # create the xml for creating the job
  file { $xml:
    ensure  => 'present',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => epp('profiles/jenkins/job.xml.epp', {
      'repo'             => $repo,
      'repo_uuid'        => $repo_uuid,
      'branch'           => $branch,
      'authtoken'        => $authtoken,
      'when'             => $when,
      'target'           => $target,
      'properties'       => profiles::jenkins_job_join_properties($properties),
      'hipchat'          => $hipchat,
      'delete_workspace' => $delete_workspace,
      'files'            => $files,
      'timeout'          => $timeout
    })
  }

  # work out the cli arguments
  $jar    = '/usr/lib/jenkins/jenkins-cli.jar'
  $site   = 'http://127.0.0.1:8080/'
  $auth   = '-ssh -user admin -i /etc/jenkins/api_id_rsa'
  $cli    = "/usr/bin/java -jar ${jar} -s ${site} ${auth}"

  # create the job for the first time
  exec { "profiles-jenkins-job-create-${name}":
    command => "${cli} create-job ${name} < ${xml}",
    unless  => "${cli} get-job ${name}",
    onlyif  => [
      '/usr/bin/test -d /var/lib/jenkins/plugins/ant',
      '/usr/bin/curl -fsSI http://127.0.0.1:8080/cli/'
    ],
    require => File[$xml],
    notify  => Exec["profiles-jenkins-job-trigger-${name}"]
  }

  # trigger a build when the job is added
  if $authtoken {
    exec { "profiles-jenkins-job-trigger-${name}":
      command     => "/usr/bin/curl http://127.0.0.1:8080/job/${name}/build?token=${authtoken}",
      onlyif      => [
        '/usr/bin/test -d /var/lib/jenkins/plugins/ant',
        '/usr/bin/curl -fsSI http://127.0.0.1:8080/cli/'
      ],
      refreshonly => true
    }
  } else {
    exec { "profiles-jenkins-job-trigger-${name}":
      command     => '/bin/true',
      refreshonly => true
    }
  }

}
