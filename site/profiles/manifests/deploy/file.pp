# == Defined Type: profiles::deploy::file

define profiles::deploy::file () {

  # profile lookups
  $source      = lookup('profiles::deploy::file::source',      String, 'first', 'puppet:///shared')
  $destination = lookup('profiles::deploy::file::destination', String)

  file { "${destination}/${name}":
    ensure => 'present',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => "${source}/${name}"
  }

}
