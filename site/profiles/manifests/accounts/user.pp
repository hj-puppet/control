# == Defined Type: profiles::accounts::user

define profiles::accounts::user (
  Variant[Boolean,Integer] $uid      = false,
  String                   $group    = $name,
  String                   $gecos    = $name,
  String                   $password = '!!',
  Array[String]            $groups   = [],
  Array[String]            $sshkeys  = [],
) {

  if $uid {

    # create the group if needed
    if $group == $name {
      profiles::accounts::group { $name:
        gid => $uid
      }
    }

    # create the user
    user { $name:
      ensure         => 'present',
      uid            => $uid,
      comment        => $gecos,
      password       => $password,
      gid            => $group,
      groups         => $groups,
      shell          => '/bin/bash',
      home           => "/home/${group}/${name}",
      purge_ssh_keys => true
    }

  } else {

    # create the group if needed
    if $group == $name {
      profiles::accounts::group { $name: }
    }

    # create the user
    user { $name:
      ensure         => 'present',
      comment        => $gecos,
      password       => $password,
      gid            => $group,
      groups         => $groups,
      shell          => '/bin/bash',
      home           => "/home/${group}/${name}",
      purge_ssh_keys => true
    }

  }

  # create the home directory
  file { "/home/${group}/${name}":
    ensure  => 'directory',
    owner   => $name,
    group   => $group,
    mode    => '0750',
    require => File[ "/home/${group}" ]
  }

  # create the .ssh directory
  file { "/home/${group}/${name}/.ssh":
    ensure  => 'directory',
    owner   => $name,
    group   => $group,
    mode    => '0700',
    require => File[ "/home/${group}/${name}" ]
  }

  # add authorized keys
  $sshkeys_hash = profiles::accounts_hash_keys($name, $sshkeys)
  $defaults = {
    ensure  => 'present',
    user    => $name,
    require => File[ "/home/${group}/${name}/.ssh" ]
  }
  each($sshkeys_hash) |$name, $parameters| {
    ssh_authorized_key { $name:
      * => $defaults + $parameters
    }
  }

}
