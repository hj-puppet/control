# == Defined Type: profiles::accounts::functional

define profiles::accounts::functional (
  String        $group    = $name,
  String        $home     = "/home/${group}/${name}",
  String        $comment  = $name,
  Array[String] $groups   = [],
  Array[String] $sshkeys  = [],
) {

  # create the group if needed
  if $group == $name {
    profiles::accounts::group { $name: }
  }

  # create the functional user
  user { $name:
    ensure         => 'present',
    comment        => $comment,
    password       => '!!',
    gid            => $group,
    groups         => $groups,
    shell          => '/bin/bash',
    home           => $home,
    purge_ssh_keys => true
  }

  # only manage home directory if it is in the standard location
  if $home == "/home/${group}/${name}" {

    # create the home directory
    file { "/home/${group}/${name}":
      ensure  => 'directory',
      owner   => $name,
      group   => $group,
      mode    => '0750',
      require => File[ "/home/${name}" ]
    }

    # create the ssh directory
    file { "/home/${group}/${name}/.ssh":
      ensure  => 'directory',
      owner   => $name,
      group   => $group,
      mode    => '0700',
      require => File[ "/home/${group}/${name}" ]
    }

    # add authorized keys
    $sshkeys_hash = profiles::accounts_hash_keys($name, $sshkeys)
    $defaults = {
      ensure  => 'present',
      user    => $name,
      require => File[ "/home/${group}/${name}/.ssh" ]
    }
    each($sshkeys_hash) |$name, $parameters| {
      ssh_authorized_key { $name:
        * => $defaults + $parameters
      }
    }

  }

}
