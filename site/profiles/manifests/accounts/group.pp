# == Defined Type: profiles::accounts::group

define profiles::accounts::group () {

  # create the group
  group { $name:
    ensure => 'present'
  }

  # create the group home directory
  file { "/home/${name}":
    ensure => 'directory',
    owner  => 'root',
    group  => $name,
    mode   => '0750'
  }

}
