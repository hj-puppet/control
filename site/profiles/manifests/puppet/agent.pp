# == Class: profiles::puppet::agent

class profiles::puppet::agent () {

  # profile lookups
  $server = lookup('profiles::puppet::agent::server', String)

  # add the Puppet 5 repo
  include 'profiles::repo::puppet5'

  # install puppet agent
  package { 'puppet-agent':
    ensure  => 'present',
    require => Class['profiles::repo::puppet5']
  }

  # create puppet.conf on puppet clients
  if ($trusted['extensions']['pp_role'] != 'puppetserver')
  and ($facts['role'] != 'puppetserver') {

    file { '/etc/puppetlabs/puppet/puppet.conf':
      ensure  => 'present',
      content => epp('profiles/puppet/puppet.conf.epp',
        {
          'server' => $server
        }
      ),
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      require => Package['puppet-agent'],
      notify  => Service['puppet']
    }

  }

  # ensure puppet service is running and enabled
  service { 'puppet':
    ensure => 'running',
    enable => true
  }

}
