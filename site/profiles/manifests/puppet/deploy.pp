# == profiles::puppet::deploy

class profiles::puppet::deploy () {

  # profile lookups
  $puppet_ca       = lookup('profiles::puppet::deploy::puppet_ca',       String)
  $gitlab_servers  = lookup('profiles::puppet::deploy::gitlab_servers',  Array[String])
  $control_repo    = lookup('profiles::puppet::deploy::control_repo',    String)
  $ca_repo         = lookup('profiles::puppet::deploy::ca_repo',         String)
  $enc_repo        = lookup('profiles::puppet::deploy::enc_repo',        String)
  $repo_key        = lookup('profiles::puppet::deploy::repo_key',        String)
  $deploy_key      = lookup('profiles::puppet::deploy::deploy_key',      String)
  $pkcs7_key       = lookup('profiles::puppet::deploy::pkcs7_key',       String)
  $repo_key_uuid   = lookup('profiles::puppet::deploy::repo_key_uuid',   String)
  $deploy_key_uuid = lookup('profiles::puppet::deploy::deploy_key_uuid', String)
  $pkcs7_key_uuid  = lookup('profiles::puppet::deploy::pkcs7_key_uuid',  String)

  # look up the list of puppet servers
  $puppet_servers = lookup('profiles::puppet::servers', Array[String])

  # allow gitlab servers to access jenkins
  profiles::firewall::rule { 'HTTPS from GitLab Servers':
    dport  => 443,
    proto  => 'tcp',
    source => $gitlab_servers
  }

  # install jenkins
  include 'profiles::jenkins'

  # configure jenkins
  class { 'profiles::jenkins::config':
    environment => {
      'PUPPET_SERVERS' => join($puppet_servers, ':'),
      'PUPPET_CA'      => $puppet_ca
    }
  }

  # install ruby
  include 'profiles::package::ruby'

  # install r10k
  package { 'r10k':
    ensure   => 'present',
    provider => 'gem',
    require  => Package['ruby']
  }

  # store the private key for connecting to the repos
  profiles::jenkins::key { 'REPO_KEY':
    uuid => $repo_key_uuid,
    key  => $repo_key
  }

  # store the deploy key for connecting to puppet servers
  profiles::jenkins::file { 'DEPLOY_KEY':
    uuid    => $deploy_key_uuid,
    content => $deploy_key
  }

  # store the PKCS7 key for decrypting CA data
  profiles::jenkins::file { 'PKCS7_KEY':
    uuid    => $pkcs7_key_uuid,
    content => $pkcs7_key
  }

  # create a pipeline for the control repo
  profiles::jenkins::pipeline { 'control':
    repo      => $control_repo,
    repo_uuid => $repo_key_uuid,
    branches  => '(production|feature-.*|release-.*|support-.*)'
  }

  # create a pipeline for the ca repo
  profiles::jenkins::pipeline { 'ca':
    repo      => $ca_repo,
    repo_uuid => $repo_key_uuid,
    branches  => 'production'
  }

  # create a pipeline for the enc repo
  profiles::jenkins::pipeline { 'enc':
    repo      => $enc_repo,
    repo_uuid => $repo_key_uuid,
    branches  => 'production'
  }

  # create a scheduled job to run the clean target in the control repo
  profiles::jenkins::job { 'clean':
    repo             => $control_repo,
    repo_uuid        => $repo_key_uuid,
    branch           => 'production',
    target           => 'clean',
    when             => 'H 23 * * *',
    delete_workspace => true,
    hipchat          => true,
    properties       => { 'servers' => '$PUPPET_SERVERS' },
    files            => { 'DEPLOY_KEY' => $deploy_key_uuid }
  }

}
