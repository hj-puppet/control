# == Defined Type: profiles::puppet::gunicorn

define profiles::puppet::gunicorn (
  Integer          $port,
  String           $owner,
  String           $group,
  String           $python_version,
  Optional[String] $appdir       = "/apps/${name}",
  Optional[String] $module       = 'config.wsgi',
  Optional[String] $requirements = undef,
  Optional[Hash]   $environment  = {}
) {

  # work out the python release
  if $python_version =~ /^(\d+)\.(\d+)$/ {
    $version_major = $1
    $version_minor = $2
    $python_release = "rh-python${version_major}${version_minor}"
  } else {
    fail("Python version is in incorrect format: ${python_version}")
  }

  # create the gunicorn startup script
  file { "/usr/local/bin/gunicorn-${name}.sh":
    ensure  => 'present',
    owner   => $owner,
    group   => $group,
    mode    => '0755',
    content => epp('profiles/puppet/gunicorn.sh.epp', {
      name           => $name,
      appdir         => $appdir,
      module         => $module,
      port           => $port,
      python_release => $python_release,
      environment    => $environment
    }),
    notify  => Service["gunicorn-${name}"]
  }

  # create the python venv for gunicorn
  exec { "profiles-puppet-gunicorn-python-venv-${name}":
    path    => "/opt/rh/${python_release}/root/usr/bin",
    user    => $owner,
    command => "python -m venv /srv/venv/${name}",
    creates => "/srv/venv/${name}",
    require => [
      File['/srv/venv'],
      Package[$python_release]
    ],
    notify  => Exec["profiles-puppet-gunicorn-python-venv-requirements-${name}"]
  }

  # install gunicorn and tornado
  exec { "profiles-puppet-gunicorn-python-venv-gunicorn-${name}":
    user    => $owner,
    command => "/srv/venv/${name}/bin/pip install gunicorn",
    creates => "/srv/venv/${name}/bin/gunicorn",
    require => Exec["profiles-puppet-gunicorn-python-venv-${name}"]
  }
  exec { "profiles-puppet-gunicorn-python-venv-tornado-${name}":
    user    => $owner,
    command => "/srv/venv/${name}/bin/pip install tornado",
    creates => "/srv/venv/${name}/lib/python3.6/site-packages/tornado",
    require => Exec["profiles-puppet-gunicorn-python-venv-${name}"]
  }

  # install the requirements.txt with pip if needed
  if $requirements {
    exec { "profiles-puppet-gunicorn-python-venv-requirements-${name}":
      command     => "/srv/venv/${name}/bin/pip install -r ${requirements}",
      refreshonly => true,
      require     => Exec["profiles-puppet-gunicorn-python-venv-${name}"]
    }
  } else {
    exec { "profiles-puppet-gunicorn-python-venv-requirements-${name}":
      command     => '/bin/true',
      refreshonly => true
    }
  }

  # create the systemd service
  file { "/usr/lib/systemd/system/gunicorn-${name}.service":
    ensure  => 'present',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => epp('profiles/puppet/gunicorn.service.epp', {
      name  => $name,
      owner => $owner,
      group => $group
    }),
    notify  => Exec["profiles-puppet-gunicorn-${name}-systemctl-daemon-reload"]
  }

  # reload systemd
  exec { "profiles-puppet-gunicorn-${name}-systemctl-daemon-reload":
    command     => '/usr/bin/systemctl daemon-reload',
    refreshonly => true,
    notify      => Service["gunicorn-${name}"]
  }

  # define the service and ensure it is enabled and running
  service { "gunicorn-${name}":
    ensure     => 'running',
    enable     => true,
    hasrestart => true,
    hasstatus  => false,
    pattern    => "/srv/venv/${name}/bin/gunicorn",
    require    => [
      File['/var/run/gunicorn'],
      File["/usr/local/bin/gunicorn-${name}.sh"],
      Exec["profiles-puppet-gunicorn-python-venv-${name}"],
      Exec["profiles-puppet-gunicorn-python-venv-gunicorn-${name}"],
      Exec["profiles-puppet-gunicorn-python-venv-tornado-${name}"]
    ]
  }

}
