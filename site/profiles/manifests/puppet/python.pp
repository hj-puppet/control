# == Class: profiles::puppet::python

class profiles::puppet::python (
  String $version
) {

  # create gunicorn group
  profiles::accounts::group { 'gunicorn': }

  # add the scl repo
  include 'profiles::repo::scl'

  # work out the python release
  if $version =~ /^(\d+)\.(\d+)$/ {
    $version_major = $1
    $version_minor = $2
    $python_release = "rh-python${version_major}${version_minor}"
  } else {
    fail("Python version is in incorrect format: ${version}")
  }

  # install python
  package { $python_release:
    ensure  => 'present',
    require => Class['profiles::repo::scl']
  }

  # create the venv and pid directories for gunicorn
  file { [
    '/srv/venv',
    '/var/run/gunicorn'
  ]:
    ensure => 'directory',
    owner  => 'root',
    group  => 'gunicorn',
    mode   => '0664'
  }

}
