# == Class: profiles::puppet::server

class profiles::puppet::server () {

  # profile lookups
  $java_ms           = lookup('profiles::puppet::server::java_ms',           String)
  $java_mx           = lookup('profiles::puppet::server::java_mx',           String)
  $autosign          = lookup('profiles::puppet::server::autosign',          Boolean)
  $deploy_ssh_pubkey = lookup('profiles::puppet::server::deploy_ssh_pubkey', String)
  $proxy_fqdn        = lookup('profiles::puppet::server::proxy_fqdn',        Optional[String], 'first', undef)


  # look up the list of puppet nodes
  $puppet_nodes = lookup('profiles::puppet::nodes', Array[String])

  # look up puppetdb server and port
  $puppetdb_server = lookup('profiles::puppet::puppetdb_server', String)
  $puppetdb_port   = lookup('profiles::puppet::puppetdb_port',   Integer)

  # look up the puppet deploy server
  $deploy_server = lookup('profiles::puppet::deploy_server', String)

  # look up the list of admin groups
  $admin_groups = lookup('profiles::admin::groups', Array, 'unique', [])

  # open the firewall for puppet nodes
  profiles::firewall::rule { 'HTTPS to puppet server':
    dport  => 8140,
    proto  => 'tcp',
    source => $puppet_nodes
  }

  # open the firewall to allow the deploy server to ssh
  profiles::firewall::rule { 'SSH from deploy server':
    dport  => 22,
    proto  => 'tcp',
    source => $deploy_server
  }

  # allow the deploy user to reinentory the Puppet CA and reload the puppet server
  profiles::sudo::rule { 'puppetserver_deploy_root':
    user     => 'deploy',
    as       => 'root',
    nopasswd => true,
    commands => [
      '/bin/systemctl reload puppetserver',
      '/opt/puppetlabs/bin/puppet cert reinventory'
    ]
  }

  # allow the deploy user to deploy CA data
  profiles::sudo::rule { 'puppetserver_deploy_puppet':
    user     => 'deploy',
    as       => 'puppet',
    nopasswd => true,
    commands => [
      '/bin/rsync --server -[lDtre.iLsfxC]* --delete --new-compress . /etc/puppetlabs/puppet/ssl/ca',
    ]
  }

  each ($admin_groups) |$index, $group| {

    # allow adming groups to reinentory the Puppet CA and reload the puppet server
    profiles::sudo::rule { "puppetserver_${group}_root":
      group    => $group,
      as       => 'root',
      nopasswd => true,
      commands => [
        '/bin/systemctl reload puppetserver',
        '/opt/puppetlabs/bin/puppet cert reinventory'
      ]
    }

    # allow admin groups to provision CA data and Eyaml keys
    profiles::sudo::rule { "puppetserver_${group}_puppet":
      group    => $group,
      as       => 'puppet',
      nopasswd => true,
      commands => [
        '/bin/rsync --server -[lDtre.iLsfxC]* --delete --new-compress . /etc/puppetlabs/puppet/ssl',
        '/bin/rsync --server -[lDtre.iLsfxC]* --delete --new-compress . /etc/puppetlabs/puppet/eyaml'
      ]
    }

  }

  # create a deploy user
  profiles::accounts::functional { 'deploy':
    sshkeys => [ $deploy_ssh_pubkey ]
  }

  # install ruby
  package { 'ruby':
    ensure => 'present'
  }

  # add the Puppet 5 repo
  include 'profiles::repo::puppet5'

  # install the Puppet server
  package { 'puppetserver':
    ensure  => 'present',
    require => Class['profiles::repo::puppet5']
  }

  # define the environments directory
  file { [
    '/etc/puppetlabs/code/environments',
    '/etc/puppetlabs/code/enc'
  ]:
    ensure  => 'directory',
    owner   => 'deploy',
    group   => 'deploy',
    mode    => '0644',
    recurse => true,
    require => Package['puppetserver']
  }

  # ensure Puppet service is running and enabled
  service { 'puppetserver':
    ensure => 'running',
    enable => true
  }

  # create puppet.conf with additional server components
  file { '/etc/puppetlabs/puppet/puppet.conf':
    ensure  => 'present',
    content => epp('profiles/puppet/server/puppet.conf.epp', {
      'server'     => $trusted['certname'],
      'proxy_fqdn' => $proxy_fqdn
    }),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => Package['puppetserver'],
    notify  => Service['puppetserver']
  }

  # create Puppet sysconfig file first
  file { '/etc/sysconfig/puppetserver':
    ensure  => 'present',
    content => epp('profiles/puppet/server/sysconfig.epp',
      {
        'java_ms' => $java_ms,
        'java_mx' => $java_mx
      }
    ),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => Package['puppetserver'],
    notify  => Service['puppetserver']
  }

  # configure autosign
  if $autosign {
    file { '/etc/puppetlabs/puppet/autosign.conf':
      ensure  => 'present',
      source  => 'puppet:///modules/profiles/puppet/server/autosign.conf',
      owner   => 'root',
      group   => 'root',
      mode    => '0755',
      require => Package['puppetserver']
    }
  } else {
    file { '/etc/puppetlabs/puppet/autosign.conf':
      ensure  => 'absent',
      require => Package['puppetserver']
    }
  }

  # install hiera-eyaml for users
  exec { 'puppet-bin-gem-hiera-eyaml':
    command => '/opt/puppetlabs/puppet/bin/gem install hiera-eyaml',
    creates => '/opt/puppetlabs/puppet/bin/eyaml',
    require => Package['puppetserver']
  }

  # install bcrypt for the puppet server
  exec { 'puppetserver-gem-bcrypt':
    command => '/opt/puppetlabs/bin/puppetserver gem install bcrypt',
    unless  => '/opt/puppetlabs/bin/puppetserver gem list -i bcrypt',
    require => Package['puppetserver'],
    notify  => Service['puppetserver']
  }

  # create a wrapper symlink for the puppet eyaml command
  file { '/opt/puppetlabs/bin/eyaml':
    ensure => 'link',
    target => '/opt/puppetlabs/puppet/bin/wrapper.sh',
    owner  => 'root',
    group  => 'root'
  }

  # create the eyaml key directory
  file { '/etc/puppetlabs/puppet/eyaml':
    ensure  => 'directory',
    owner   => 'puppet',
    group   => 'puppet',
    mode    => '0750',
    require => Package['puppetserver']
  }

  # create the eyaml config directory
  file { '/etc/eyaml':
    ensure => 'directory',
    owner  => 'root',
    group  => 'root',
    mode   => '0755'
  }

  # deploy the eyaml config file
  file { '/etc/eyaml/config.yaml':
    ensure  => 'present',
    source  => 'puppet:///modules/profiles/puppet/server/eyaml-config.yaml',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => File['/etc/eyaml']
  }

  # create the hiera config file
  file { '/etc/puppetlabs/puppet/hiera.yaml':
    ensure  => 'present',
    source  => 'puppet:///modules/profiles/puppet/server/hiera.yaml',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => Package['puppetserver'],
    notify  => Service['puppetserver']
  }

  # symlink the system hiera.yaml file to the Puppet one
  file { '/etc/hiera.yaml':
    ensure => 'link',
    target => '/etc/puppetlabs/puppet/hiera.yaml',
    owner  => 'root',
    group  => 'root'
  }

  # create the enc executable
  file { '/etc/puppetlabs/puppet/enc.rb':
    ensure  => 'present',
    source  => 'puppet:///modules/profiles/puppet/server/enc.rb',
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    require => Package['puppetserver'],
    notify  => Service['puppetserver']
  }

  # create the fileserver paths
  file { [
    '/srv/puppetserver',
    '/srv/puppetserver/shared'
  ]:
    ensure  => 'directory',
    owner   => 'puppet',
    group   => 'puppet',
    mode    => '2770',
    require => Package['puppetserver']
  }

  # create the puppet fileserver config
  file { '/etc/puppetlabs/puppet/fileserver.conf':
    ensure  => 'present',
    source  => 'puppet:///modules/profiles/puppet/server/fileserver.conf',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => Package['puppetserver'],
    notify  => Service['puppetserver']
  }

  # create the auth.conf file for the puppet fileserver
  file { '/etc/puppetlabs/puppetserver/conf.d/auth.conf':
    ensure  => 'present',
    source  => 'puppet:///modules/profiles/puppet/server/auth.conf',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => Package['puppetserver'],
    notify  => Service['puppetserver']
  }

  # configure puppetdb
  class { 'puppetdb::master::config':
    puppetdb_server   => $puppetdb_server,
    puppetdb_port     => $puppetdb_port,
    strict_validation => false,
    require           => Package['puppetserver']
  }

}
