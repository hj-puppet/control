# == Defined Type: profiles::firewall::accept
#
# Private defined type used by profiles::firewall::rule and
# profiles::firewall::rule6 to accept traffic on a port or ports
# either from a single IP address range or from anywhere.
#
# === Parameters
#
# [*name*]
#   (Namevar) The name of the firewall rule.
#
# [*dport*]
#   (Required) The port or ports to open.
#
# [*proto*]
#   (Required) Protocol to use.
#
# [*provider*]
#   (Optional) Which provider to use.
#
# [*source*]
#   (Optional) IP address or single IP address range.
#

define profiles::firewall::accept (
  Variant[Integer, Array[Integer]] $dport,
  String                           $proto,
  String                           $provider = 'iptables',
  Variant[Boolean, String]         $source = false
)  {

  if $source {

    firewall { "100 Allow inbound ${name}":
      dport    => $dport,
      proto    => $proto,
      source   => profiles::resolve($source),
      provider => $provider,
      action   => 'accept'
    }

  } else {

    firewall { "100 Allow inbound ${name}":
      dport    => $dport,
      proto    => $proto,
      provider => $provider,
      action   => 'accept'
    }

  }

}
